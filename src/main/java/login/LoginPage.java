package login;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import store.StorePage;

public class LoginPage {
    private By usernameInput = By.xpath("//input[@id='user-name']");
    private By passwordInput = By.xpath("//input[@id='password']");
    private By submitBtn = By.xpath("//input[@class='btn_action']");
    private By errorMsg = By.xpath("//h3[@data-test='error']");
    private By closeErrorIcon = By.xpath("//button[@class='error-button']");

    WebDriver driver;

    public LoginPage(WebDriver driver) {
        this.driver = driver;
    }

    //Are the objects visible?
    public boolean isUsernameInputDisplayed() {
        return driver.findElement(usernameInput).isDisplayed();
    }

    public boolean isPasswordInputDisplayed() {
        return driver.findElement(passwordInput).isDisplayed();
    }

    public boolean isSubmitBtnDisplayed() {
        return driver.findElement(submitBtn).isDisplayed();
    }

    public boolean isErrorMessageDisplayed() {
        return driver.findElements(errorMsg).size() != 0;
    }
    //Describe actions with objects

    public LoginPage typeUsername(String username) {
        driver.findElement(usernameInput).clear();
        driver.findElement(usernameInput).sendKeys(username);
        return this;
    }

    public LoginPage typePassword(String password) {
        driver.findElement(passwordInput).clear();
        driver.findElement(passwordInput).sendKeys(password);
        return this;
    }

    public LoginPage closeError() {
        driver.findElement(closeErrorIcon).click();
        return this;
    }

    public String getErrorMsg() {
        return driver.findElement(errorMsg).getText();
    }

    public StorePage confirm() {
        driver.findElement(submitBtn).click();
        return new StorePage(driver);
    }
}
