package store;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class StorePage {
    WebDriver driver;

    //Elements
    private By pageHeader = By.xpath("//div[@class='product_label']");
    private By dropSortItemsList = By.xpath("//select[@class='product_sort_container']");
    private By imageOfProducts = By.xpath("//img[@class='inventory_item_img']");
    //Drop list options
    private String dropListOptions = "//select[@class='product_sort_container']/option[text()=\"%s\"]";

    public StorePage(WebDriver driver) {
        this.driver = driver;
    }

    //Actions with header text
    public String getHederText() {
        return driver.findElement(pageHeader).getText();
    }

    //Actions with drop list
    public boolean isDropSortItemsListEnabled() {
        return driver.findElements(dropSortItemsList).size() != 0;
    }

    public StorePage clickToTheDropList() {
        driver.findElement(dropSortItemsList).click();
        return this;
    }

    public String getOptionsText(String optionName) {
        return driver.findElement(By.xpath(String.format(dropListOptions, optionName))).getText();
    }

    //Actions with images
    public boolean isImageLoaded(WebElement imgElement) {
        Boolean isImageLoaded = (Boolean) ((JavascriptExecutor)driver).
                executeScript("return arguments[0].complete && typeof arguments[0].naturalWidth != " +
                        "\"undefined\" && arguments[0].naturalWidth > 0", imgElement);
        return isImageLoaded;
    }

    public boolean areImagesLoaded() {
        List<WebElement> imgs = driver.findElements(imageOfProducts);
        boolean status = false;
        for(WebElement img : imgs) {
            status = isImageLoaded(img);
        }
        return status;
    }

}
