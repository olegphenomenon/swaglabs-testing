package tools;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.coordinates.WebDriverCoordsProvider;
import ru.yandex.qatools.ashot.shooting.ShootingStrategies;

import javax.imageio.ImageIO;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Screenshoter {
    private Screenshot actualScreenshot;
    private Screenshot expectedScreenshot;

    private File actualFile;
    private File expectedFile;

    private WebDriver driver;

    public static final String PATH_TO_ACTUAL_SCREENSHOTS = "testScreenshots/actual/";
    public static final String PATH_TO_EXPECTED_SCREENSHOTS = "testScreenshots/expected/";
    public static final String PATH_TO_DIFF_SCREENSHOTS = "screenshots/diff/";
    public static final String PATH_TO_GIF_SCREENSHOTS = "screenshots/gifs/";

    public Screenshoter(WebDriver driver) {
        this.driver = driver;
    }

    protected String generateFileName() {
        return new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
    }

    public void makeScreenshot(String filename) {
        filename = filename + generateFileName();
        this.actualScreenshot = new AShot()
                .shootingStrategy(ShootingStrategies.viewportPasting(100)).takeScreenshot(driver);
        this.actualFile = new File(PATH_TO_ACTUAL_SCREENSHOTS + filename + ".png");

        try {
            ImageIO.write(this.actualScreenshot.getImage(), "png", this.actualFile);
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        this.saveActualScreenshotAsExpectedIfExpectedDoesNotExist(filename);
    }

    public void makeElementScreenshot(String filename, String elementXPath) {

        filename = filename + generateFileName();

        WebElement element = driver.findElement(By.xpath(elementXPath));

        this.actualScreenshot = new AShot()
                .shootingStrategy(ShootingStrategies.viewportPasting(100))
                .coordsProvider(new WebDriverCoordsProvider())
                .takeScreenshot(driver, element);

        this.actualFile = new File(PATH_TO_ACTUAL_SCREENSHOTS + filename + ".png");

        try {
            ImageIO.write(this.actualScreenshot.getImage(), "png", this.actualFile);
        } catch(Exception e) {
            e.printStackTrace();
        }

        this.saveActualScreenshotAsExpectedIfExpectedDoesNotExist(filename);
    }

    public void makeElementScreenshot(String filename, WebElement element) {
        filename = filename + generateFileName();

        this.actualScreenshot = new AShot()
                .shootingStrategy(ShootingStrategies.viewportPasting(100))
                .coordsProvider(new WebDriverCoordsProvider())
                .takeScreenshot(driver, element);
        this.actualFile = new File(PATH_TO_ACTUAL_SCREENSHOTS + filename + ".png");

        try {
            ImageIO.write(this.actualScreenshot.getImage(), "png", this.actualFile);
        } catch(Exception e) {
            e.printStackTrace();
        }

        this.saveActualScreenshotAsExpectedIfExpectedDoesNotExist(filename);
    }

    private void saveActualScreenshotAsExpectedIfExpectedDoesNotExist(String filename) {
        filename = filename + generateFileName();

        this.expectedFile = new File(PATH_TO_EXPECTED_SCREENSHOTS + filename + ".png");

        if(!expectedFile.exists()) {
            this.expectedScreenshot = this.actualScreenshot;
            try {
                ImageIO.write(this.actualScreenshot.getImage(), "png", this.actualFile);
            } catch(Exception e) {
                e.printStackTrace();
            }
        } else {
            try {
                this.expectedScreenshot = new Screenshot(ImageIO.read(expectedFile));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void compareImages() {

    }

    public Screenshot getActualScreenshot() {
        return this.actualScreenshot;
    }

    public Screenshot getExpectedScreenshot() {
        return this.expectedScreenshot;
    }

    public static void removeActualScreenshot() {
        File directory = new File(PATH_TO_ACTUAL_SCREENSHOTS);
        try {
            FileUtils.cleanDirectory(directory);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void removeExpectedScreenshot() {
        File directory = new File(PATH_TO_EXPECTED_SCREENSHOTS);
        try {
            FileUtils.cleanDirectory(directory);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public static void removeDiffScreenshot() {
        File directory = new File(PATH_TO_DIFF_SCREENSHOTS);
        try {
            FileUtils.cleanDirectory(directory);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public static void removeGifScreenshot() {
        File directory = new File(PATH_TO_GIF_SCREENSHOTS);
        try {
            FileUtils.cleanDirectory(directory);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
}
