package login;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;


public class LoginTest {

    final static Logger logger = LogManager.getLogger(LoginTest.class);

    LoginPage loginPage;
    WebDriver driver;

    private String standardUser = "standard_user";
    private String lockedOutUser = "locked_out_user";
    private String problemUser = "problem_user";
    private String password = "secret_sauce";

    private String usernameRequiredError = "Epic sadface: Username is required";
    private String passwordRequiredError = "Epic sadface: Password is required";
    private String wrongAccount = "Epic sadface: Username and password do not match any user in this service";
    private String lockedOutUserMessage = "Epic sadface: Sorry, this user has been locked out.";

    @Before
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "C:\\drivers\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        loginPage = new LoginPage(driver);
    }

    //Test case: SwagLabs-UI-1 | Are the objects displaying?
    @Ignore
    @Test
    public void areTheObjectsDisplaying() {
        driver.navigate().to("https://www.saucedemo.com/index.html");
        Assert.assertTrue(loginPage.isUsernameInputDisplayed());
        Assert.assertTrue(loginPage.isPasswordInputDisplayed());
        Assert.assertTrue(loginPage.isSubmitBtnDisplayed());
    }

    //Test case: SwagLabs-UI-2 | Sign in with standard user
    @Ignore
    @Test
    public void signInWithStandartUser() {
        driver.navigate().to("https://www.saucedemo.com/index.html");
        loginPage.typeUsername(standardUser).typePassword(password).confirm();
        Assert.assertEquals("https://www.saucedemo.com/inventory.html", driver.getCurrentUrl());
    }

    //Test case: SwagLabs-UI-3 | Sign in with locked out user
    @Ignore
    @Test
    public void signInWithLockedOutUser() {
        driver.navigate().to("https://www.saucedemo.com/index.html");
        loginPage.typeUsername(lockedOutUser).typePassword(password).confirm();
        Assert.assertEquals(lockedOutUserMessage, loginPage.getErrorMsg());
    }
    
    //Test case: SwagLabs-UI-4 | Sign in with problem user
    @Ignore
    @Test
    public void signInWithProblemUser() {
        driver.navigate().to("https://www.saucedemo.com/index.html");
        loginPage.typeUsername(problemUser).typePassword(password).confirm();
        Assert.assertEquals("https://www.saucedemo.com/inventory.html", driver.getCurrentUrl());
    }

    //Test case: SwagLabs-UI-5 | Sign in with empty fields
    @Ignore
    @Test
    public void signInWithEmptyFields(){
        driver.navigate().to("https://www.saucedemo.com/index.html");
        loginPage.typeUsername("").typePassword("").confirm();
        Assert.assertEquals(usernameRequiredError, loginPage.getErrorMsg());
    }

    //Test case: SwagLabs-UI-6 | Sign in with empty username field
    @Ignore
    @Test
    public void signInWithEmptyUsernameField(){
        driver.navigate().to("https://www.saucedemo.com/index.html");
        loginPage.typeUsername("").typePassword(password).confirm();
        Assert.assertEquals(usernameRequiredError, loginPage.getErrorMsg());
    }

    //Test case: SwagLabs-UI-7 | Sign in with empty password field
    @Ignore
    @Test
    public void signInWithEmptyPasswordField(){
        driver.navigate().to("https://www.saucedemo.com/index.html");
        loginPage.typeUsername(standardUser).typePassword("").confirm();
        Assert.assertEquals(passwordRequiredError, loginPage.getErrorMsg());
    }

    //Test case: SwagLabs-UI-8 | Sign in with wrong username
    @Ignore
    @Test
    public void signInWithWrongUsername() {
        driver.navigate().to("https://www.saucedemo.com/index.html");
        loginPage.typeUsername("Mr. Robot").typePassword("F### Socium").confirm();
        logger.info("Error message expected: " + wrongAccount);
        logger.info("Error message actual: " + loginPage.getErrorMsg());
        Assert.assertEquals(wrongAccount, loginPage.getErrorMsg());
    }

    //Test case: SwagLabs-UI-9 | Sign in with wrong password for valid username
    @Ignore
    @Test
    public void signInWithInvalidPassword() {
        driver.navigate().to("https://www.saucedemo.com/index.html");
        loginPage.typeUsername(standardUser).typePassword("wrong_password").confirm();
        logger.info("Error message expected: " + wrongAccount);
        logger.info("Error message actual: " + loginPage.getErrorMsg());
        Assert.assertEquals(wrongAccount, loginPage.getErrorMsg());
    }

    //Test case: SwagLabs-UI-10 | close error message
    @Test
    public void closeErrorMessage() {
        driver.navigate().to("https://www.saucedemo.com/index.html");
        loginPage.typeUsername(standardUser).typePassword("wrong_password").confirm();
        Assert.assertTrue(loginPage.isErrorMessageDisplayed());
        loginPage.closeError();
        Assert.assertFalse(loginPage.isErrorMessageDisplayed());
    }

    @After
    public void tearDown() {
        driver.quit();
    }
}
