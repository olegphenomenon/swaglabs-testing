package store;

import login.LoginPage;
import org.junit.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.shooting.ShootingStrategies;
import ru.yandex.qatools.ashot.shooting.ShootingStrategy;
import tools.Screenshoter;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class StoreTest {
    private WebDriver driver;
    private StorePage storePage;
    private LoginPage loginPage;

    private String standardUser = "standard_user";
    private String password = "secret_sauce";

    private String pageHeader = "Products";

    private HashMap<String, String> sortListOptions = new HashMap<String, String>();

    Screenshoter screenshoter;



    @Before
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "C:\\drivers\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
        driver.manage().window().maximize();

        storePage = new StorePage(driver);
        loginPage = new LoginPage(driver);

        sortListOptions.put("sortByIncName", "Name (A to Z)");
        sortListOptions.put("sortByDecName", "Name (Z to A)");
        sortListOptions.put("sortByIncPrice", "Price (low to high)");
        sortListOptions.put("sortByDecPrice", "Price (high to low)");

        screenshoter = new Screenshoter(driver);

        driver.navigate().to("https://www.saucedemo.com/index.html");
        loginPage.typeUsername(standardUser).typePassword(password).confirm();
    }

    // Does the elements displayed?
    @Test
    public void areElementsEnabled() {
        Assert.assertEquals(storePage.getHederText(), pageHeader);

        Assert.assertTrue(storePage.isDropSortItemsListEnabled());

        storePage.clickToTheDropList();
        for(Map.Entry<String, String> entry : sortListOptions.entrySet()) {
            Assert.assertEquals(storePage.getOptionsText(entry.getValue()), entry.getValue());
        }

        Assert.assertTrue(storePage.areImagesLoaded());

//        screenshoter.makeScreenshot("test");
    }

    @After
    public void tearDown() {
        driver.quit();
    }

}
